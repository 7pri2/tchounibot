var utils = require('./utils');

module.exports = [
	function(words) {
		first = utils.random(words);
		return utils.upperfirst(first.le_mot()) + " ? Oui ! Mais " + first.le_mot() + " contractuel"+ (first.est_feminin() ? "le" : "") +" !";
	},
	function(words) {
		first = utils.random(words);
		return "Si pas de bidouille (genre " + first.le_mot() + "), pas de risque.";
	},
	function(words) {
		first = utils.random(words);
		return "Pourquoi ? Par respect du principe " + first.preposition('de') + " !";
	},
	function(words) {
		first = utils.random(words);
		return "Attention ! Il y a des cas (tordus) dans lesquels " + first.definis() + " peut avoir de l'importance";
	},
	function(words) {
		first = utils.random(words);
		return "Si pas de bidouille (genre " + first.le_mot() + "), pas de risque.";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		third = utils.random(words);
		fourth = utils.random(words);
		return utils.upperfirst(first.definis()) + " est " + second.preposition('au') + " ce que " + third.definis() + " est " + fourth.preposition('au') + ".";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return utils.upperfirst(first.le_mot()) + " ou " + second.le_mot() + " ? Ni l'un ni l'autre : les deux provoquent une erreur !";
	},
	function(words) {
		first = utils.random(words);
		return utils.upperfirst(first.partitif()) + " oui, mais " + first.partitif() + " CONTRAINT" + (first.est_feminin() ? "E" : "") + " !";
	}
]