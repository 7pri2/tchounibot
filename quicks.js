var utils = require('./utils');

module.exports = [
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "En quoi " + first.definis() + " représente-t-" + first.troisieme_personne() + " " + second.indefinis() + " ?";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "En quoi " + first.definis() + " est-" + first.troisieme_personne() + " une forme " + second.preposition('de') + " ?";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "En quoi " + first.definis() + " permet-t-" + first.troisieme_personne() + " de faire " + second.partitif() + " ?";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "En quoi " + first.definis() + " est-" + first.troisieme_personne() + " un facteur limitant " + second.definis() + " ?";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "Peut-on dire que " + first.definis() + " dépend " + second.partitif() + " ?";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "Exprimez de quelle manière " + first.definis() + " modélise " + second.definis() + ".";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "En quoi " + first.definis() + " est une autre forme " + second.preposition('de') + " ?";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "En quoi " + first.definis() + " utilise-t-" + first.troisieme_personne() + " le principe " + second.preposition('de') + " ?";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "Expliquez comment " + first.definis() + " sous-entend l'utilisation " + second.partitif() + ".";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		return "Expliquez pourquoi " + first.definis() + " ne saurait être " + second.partitif() + ".";
	}
]