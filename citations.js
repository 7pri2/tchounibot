var utils = require('./utils');

module.exports = [
	function(words) {
		first = utils.random(words);
		return utils.upperfirst(first.mot()) + " ? Oui ! Mais " + utils.upperfirst(first.mot()) + ".";
	},
	function(words) {
		first = utils.random(words);
		return "Si pas de bidouille (genre " + first.mot() + "), pas de risque.";
	},
	function(words) {
		first = utils.random(words);
		return "Pourquoi ? Par respect du principe " + first.preposition('de') + " !";
	},
	function(words) {
		first = utils.random(words);
		return "Attention ! Il y a des cas (tordus) dans lesquels " + first.indefinis() + " peut avoir de l'importance";
	},
	function(words) {
		first = utils.random(words);
		return "Si pas de bidouille (genre " + first.mot() + "), pas de risque.";
	},
	function(words) {
		first = utils.random(words);
		second = utils.random(words);
		third = utils.random(words);
		fourth = utils.random(words);
		return utils.upperfirst(first.definis()) + " est " + second.preposition('au') + " ce que " + third.definis() + " est " + fourth.preposition('au') + ".";
	},
	function(words) {
		first = utils.random(words);
		return "Si pas de bidouille (genre " + first.mot() + "), pas de risque.";
	}
]