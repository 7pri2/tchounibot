var Discord = require('discord.io');
var logger = require('winston');
var auth = require('./auth.json');
var buzz = require('./buzzword');
var vocab = require('./vocab.json');
var quicks = require('./quicks');
var quotes = require('./quotes');
var utils = require('./utils');

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});

logger.level = 'debug';

// Initialize Discord Bot
var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});

function xinspect(o,i){
    if(typeof i=='undefined')i='';
    if(i.length>50)return '[MAX ITERATIONS]';
    var r=[];
    for(var p in o){
        var t=typeof o[p];
        r.push(i+'"'+p+'" ('+t+') => '+(t=='object' ? 'object:'+xinspect(o[p],i+'  ') : o[p]+''));
    }
    return r.join(i+'\n');
}

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});

// On initialise les objets
// Buzzwords
var words = [];
vocab.forEach(function(o) {
	words.push(new buzz.Buzzword(o));
});

bot.on('message', function (user, userID, channelID, message, evt) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`
    if (message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
       
        args = args.splice(1);
        switch(cmd) {
            // !ping
            case 'ping':
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });
            break;
            case 'tchounibot':
                bot.sendMessage({
                    to: channelID,
                    message: "Bondoufle ! Je suis Pierre Tchounibot ! Vous pouvez me demander des questions de quicks avec la commande `!tchouniquick` ou je peux vous resortir mes citations préférées sur la conception orientée objets grâce à la commande `!tchouniquote` !"
                });
                break;
            case 'tchouniquick':
            	bot.sendMessage({
            		to: channelID,
            		message: utils.random(quicks)(words)
            	});
            	break;
        	case 'tchouniquote':
	        	bot.sendMessage({
	        		to: channelID,
	        		message: utils.random(quotes)(words)
	        	});
            	break;
         }
     }
});