module.exports = {
	random : function(tab) {
		return tab[Math.floor(Math.random() * tab.length)];
	},
	upperfirst : function(word) {
		return word.charAt(0).toUpperCase() + word.slice(1);
	}
}