est_voyelle = function(c) {
	switch(c) {
	case 'a':
	case 'e':
	case 'i':
	case 'o':
	case 'u':
	case 'y':
	case 'h':	return true;
	default:	return false;
	}
}

pluriels = function(w) {
	switch(w) {
	case 'le':	return "les";
	case 'la':	return "les";
	case 'l\'':	return "les";
	case 'les':	return "les";
	case 'un':	return "des";
	case 'une':	return "des";
	case 'des':	return "des";
	case 'du':	return "des";
	case 'de la':	return "des";
	case 'de l\'':	return "des";
	case 'ce':	return "ces";
	case 'cette':	return "ces";
	case 'cet':	return "ces";
	case 'ces':	return "ces";
	}
}

module.exports = {
	Buzzword : class {
		constructor(config) {
			this.feminin = config.feminin;
			this.mot = config.mot;
			pluriels("le");
		}

		definis(pluriel = false) {
			if(pluriel)
				return pluriels(this.p_definis()) + this.mot;
			else
				return this.p_definis() + this.mot;
		}

		indefinis(pluriel = false) {
			if(pluriel)
				return pluriels(this.p_indefinis()) + " " + this.mot;
			else
				return this.p_indefinis() + " " + this.mot;
		}
		
		demonstratif(pluriel = false) {
			if(pluriel)
				return pluriels(this.p_demonstratif()) + " " + this.mot;
			else
				return this.p_demonstratif() + " " + this.mot;
		}
		
		partitif(pluriel = false) {
			return this.p_partitif() + this.mot;
		}
		
		preposition(type) {
			return this.p_preposition(type) + this.mot;
		}
		
		le_mot() {
			return this.mot;
		}

		est_feminin() {
			return this.feminin;
		}
		
		troisieme_personne(pluriel = false) {
			if(pluriel)
				return pluriels[this.p_trois_personne()];
			else
				return this.p_troisieme_personne();
		}
		
		p_definis() {
			if(est_voyelle(this.mot[0]))
				return "l'";
			else if(this.feminin)
				return "la ";
			else
				return "le ";
		}

		p_indefinis() {
			if(this.feminin)
				return "une";
			else
				return "un";
		}
		
		p_demonstratif() {
			if(this.feminin)
				return "cette";
			else if(est_voyelle(this.mot[0]))
				return "cet";
			else
				return "ce";
		}
		
		p_partitif() {
			if(est_voyelle(this.mot[0]))
				return "de l'";
			else if(this.feminin)
				return "de la ";
			else
				return "du ";
		}
		
		p_preposition(type) {
			switch(type) {
			case 'de':
				if(est_voyelle(this.mot[0])) 
					return "d'";
				else
					return "de ";
			case 'au':
				if(est_voyelle(this.mot[0]))
					return "à l'";
				else if(this.feminin)
					return "à la ";
				else
					return "au "
			}
		}
		
		p_troisieme_personne() {
			if(this.feminin)
				return "elle";
			else
				return "il";
		}
	}
}